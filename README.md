# [TextMatch](http://maxtextmatch.azurewebsites.net) #

Implemented in asp.net vNext.

+ I tried to be as true to my normal coming style as possible, but that was quite difficult without using Linq. 
+ It wasn't clear from the specification what the return value should be for no matches. I have used null. Normally I would clarify something like this.
+ I wouldn't normally use such a cutting edge asp.net version for production code, but my windows laptop was stolen so I've used the tools which are more suited for my environment.
+ I didn't get time to add any end to end tests. In a larger project I would prefer to add some of these at the beginning as i feel that the initial time investment is more than repaid by the confidence it gives and the flexibility that allows.

I look forward to hearing your thoughts.