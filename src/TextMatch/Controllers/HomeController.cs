using Microsoft.AspNet.Mvc;
using TextMatch.Models;

namespace TextMatch.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View(new TextMatcher());
        }
        
        [HttpPost]
        public IActionResult Match(TextMatcher match)
        {
            return View(match);
        }
        
        public IActionResult Error()
        {
            return View();
        }
    }
}
