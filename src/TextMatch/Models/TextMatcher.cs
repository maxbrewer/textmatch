using System.Collections.Generic;

namespace TextMatch.Models
{
    public class TextMatcher
    {
        private const string NoMatchMessage = null;
        
        public TextMatcher() : this(null, null) { }
        
        public TextMatcher(string text, string subtext)
        {
            Text = text;
            Subtext = subtext;
        }
        
        public string Text { get; set; }
        public string Subtext { get; set; }
        public string Matches { get { return Match(Text, Subtext); } }
    
        private static string Match(string text, string subtext)
        {
            if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(subtext))
                return NoMatchMessage;
            
            var matches = new List<string>();
            
            var lowerText = text.ToLowerInvariant();
            var lowerSubtext = subtext.ToLowerInvariant();
            
            for (int textIndex = 0; textIndex < text.Length; textIndex++)
            {
                if (!SubtextMatchesAtIndex(textIndex, lowerText, lowerSubtext))
                    continue;
                
                var matchedAt = textIndex + 1;
                matches.Add(matchedAt.ToString());
            }
            
            if (matches.Count == 0)
                return NoMatchMessage;
                
            return string.Join(",", matches);
        }
                
        private static bool SubtextMatchesAtIndex(int startIndex, string text, string subtext)
        {
            if (startIndex + subtext.Length > text.Length)
                return false;
            
            var textIndex = startIndex;
            
            foreach (var character in subtext)
                if (text[textIndex++] != character)
                    return false;
            
            return true;
        }
    }
}