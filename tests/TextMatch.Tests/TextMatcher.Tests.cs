using Xunit;
using TextMatch.Models;

namespace TestMatch.Tests
{
    public class TextMatcherTests
    {
        public const string SampleText = "Polly put the kettle on, polly put the kettle on, polly put the kettle on we’ll all have tea";
        
        [Theory]
        [InlineData(SampleText, null, null)]
        [InlineData(SampleText, SampleText, "1")]
        [InlineData("abcdefw", "w", "7")]
        [InlineData("awbwcw", "w", "2,4,6")]
        [InlineData("awbwwcw", "ww", "4")]
        [InlineData("abc", "", null)]
        [InlineData("abcdefw", "W", "7")]
        [InlineData(SampleText, "Polly", "1,26,51")]
        [InlineData(SampleText, "ll", "3,28,53,78,82")]
        [InlineData(SampleText, "X", null)]
        [InlineData(SampleText, "Polx", null)]
        [InlineData(null, "anything", null)]
        public void TextMatchGivesDesiredOutput(string text, string subtext, string expectedOutput)
        {
            var textMatcher = new TextMatcher(text, subtext);
            Assert.Equal(expectedOutput, textMatcher.Matches);
        }
    }
}
